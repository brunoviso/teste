CREATE PROC SP_UP_LOTE
( 
   @pCOD_LOTE               TP_COD_LOTE,
   @pCOD_RESULTANTE_PROCESSO        TP_COD_RESULT_PROCESSO  , 
   @pCOD_PARAMETRO_ENVASE           TP_COD_PARAM_ENVASE     ,   
   @pCOD_LINHA_PRODUCAO             TP_COD_LINHA_PRODUCAO   ,  
   @pCOD_PONTO_PRODUTIVO            TP_COD_PONTO_PRODUTIVO  ,      
   @pCOD_TIPO_LOTE                  TP_COD_TIPO_LOTE        ,   
   @pCOD_SITUACAO_LOTE              TP_COD_SITUACAO_LOTE    ,  
   @pCOD_UNB                        TP_COD_UNB              ,   
   @pCOD_GRUPO_LOTE                 TP_COD_GUPO_LOTE        ,  
   @pCOD_UNB_REMETENTE              TP_COD_UNB              ,   
   @pCOD_LGN_USU                    varchar(30)             ,   
   @pNUM_EXTERNO_LOTE               int                 ,
   @pANO_LOTE                       smallint            , 
   @pDAT_INICIO_LOTE                datetime            ,  
   @pDAT_FIM_LOTE                   datetime            ,   
   @pDAT_LIBERACAO_LOTE             datetime            , 
   @pDSC_LOTE                       TP_DESCRICAO        ,  
   @pIND_NAO_CONFORMIDADE           TP_IND              , 
   @pIND_NAO_CONFORMIDADE_ORI       TP_IND          ,
   @pCOD_TAMANHO_LOTE               smallint =null          ,
   @pCOD_FORNECEDOR                 char(6)  =null          ,
   @pCOD_RESULTADO_INSPECAO_LOTE    smallint =null 
)
As 

Declare @dat_fim_lote           datetime
Declare @cod_resultante_processo    tp_cod_result_processo
Declare @dat_historico          datetime
Declare @cod_fornecedor         char(6)
Declare @cod_resultado_inspecao_lote    smallint
Declare @cod_tamanho_lote       smallint

----------------------------------------------
--	PROJETO REDESENHO MES - ATIVIDADE: 1.6
DECLARE	@ERROR		INT,
	@COD_SIS	CHAR(2),
	@COD_ERR	CHAR(5),
	@DSC_ERR	VARCHAR(255)
----------------------------------------------

SELECT @pCOD_LGN_USU = LTRIM(RTRIM(host_name()))
 
--verifica se a linha de produþÒo pertence a unb do lote
if not exists(select 1
                from linha_producao
  where cod_linha_producao = @pcod_linha_producao
                 and cod_unb = @pcod_unb 
                 and dat_exc is null)
   BEGIN
      EXEC SP_SE_ERRO 'AD','30000','Linha de produþÒo tem que pertencer a mesma UNB do lote' 
      return 
   END 

-- verifica se o ponto produtivo esta associado a linha de produþÒo do lote
if @pcod_ponto_produtivo is not null
   if not exists(select 1
                   from linha_producao_ponto_produtivo
                  where cod_linha_producao = @pcod_linha_producao
                    and cod_ponto_produtivo = @pcod_ponto_produtivo
                    and dat_exc is null)
      BEGIN
         EXEC SP_SE_ERRO 'AD','30000','O Ponto Produtivo tem que estar associado a Linha de ProduþÒo' 
         return 
      END   

-- Obtem data final do lote (atual), resultante de processo, fornecedor,resultado da inspeþÒo e tamanho_lote

select @dat_fim_lote = null
select @cod_resultante_processo = null
select @cod_fornecedor = null
select @cod_resultado_inspecao_lote = null
select @cod_tamanho_lote = null

select @dat_fim_lote = dat_fim_lote,
       @cod_resultante_processo = cod_resultante_processo,
     @cod_fornecedor = cod_fornecedor,
     @cod_resultado_inspecao_lote = cod_resultado_inspecao_lote,
     @cod_tamanho_lote = cod_tamanho_lote
    from lote
    where lote.cod_lote = @pcod_lote

/********************************************************************************/
--	PROJETO REDESENHO MES - ATIVIDADE: 1.6
--	CHAMADA DA PROCEDURE DE CHECK DE INTEGRIDADE
	EXEC SP_CK_LOTE	@PCOD_LINHA_PRODUCAO,
			@PCOD_PONTO_PRODUTIVO,
			@PCOD_TIPO_LOTE,
			@PCOD_SITUACAO_LOTE,
			@PCOD_UNB,
			@PCOD_UNB_REMETENTE,
			@PCOD_GRUPO_LOTE,
			@PCOD_LGN_USU,
			@PCOD_PARAMETRO_ENVASE,
			@PCOD_RESULTANTE_PROCESSO,
			@PCOD_FORNECEDOR,
			@PCOD_RESULTADO_INSPECAO_LOTE,

			@ERROR OUTPUT,
			@COD_SIS OUTPUT,
			@COD_ERR OUTPUT,
			@DSC_ERR OUTPUT

	IF @ERROR = -1
	BEGIN
		EXEC SP_SE_ERRO @COD_SIS, @COD_ERR, @DSC_ERR
		RETURN
	END
/********************************************************************************/

-- Cria tabela para armazenar os itens desassociados a nova resultante
-- MOVIDA PARA FORA DA TRANSACAO
   create table #item_resultante (cod_coleta integer null, dsc_coleta char(1) null)

    BEGIN TRANSACTION TRANS_LOTE 

        UPDATE  LOTE    SET
            COD_RESULTANTE_PROCESSO     = @pCOD_RESULTANTE_PROCESSO,
            COD_PARAMETRO_ENVASE        = @pCOD_PARAMETRO_ENVASE,
            COD_LINHA_PRODUCAO      = @pCOD_LINHA_PRODUCAO,
            COD_PONTO_PRODUTIVO     = @pCOD_PONTO_PRODUTIVO,        
            COD_TIPO_LOTE           = @pCOD_TIPO_LOTE,
            COD_SITUACAO_LOTE           = @pCOD_SITUACAO_LOTE,
            COD_UNB             = @pCOD_UNB,
            COD_GRUPO_LOTE          = @pCOD_GRUPO_LOTE,
            COD_UNB_REMETENTE           = @pCOD_UNB_REMETENTE,
            COD_LGN_USU             = @pCOD_LGN_USU,
            NUM_EXTERNO_LOTE            = @pNUM_EXTERNO_LOTE,
            ANO_LOTE                = @pANO_LOTE,
            DAT_INICIO_LOTE         = @pDAT_INICIO_LOTE,
            DAT_FIM_LOTE            = @pDAT_FIM_LOTE,
            DAT_LIBERACAO_LOTE      = @pDAT_LIBERACAO_LOTE,
            DSC_LOTE                = @pDSC_LOTE,
            IND_NAO_CONFORMIDADE        = @pIND_NAO_CONFORMIDADE,
            IND_NAO_CONFORMIDADE_ORI    = @pIND_NAO_CONFORMIDADE_ORI,
              COD_TAMANHO_LOTE          = @pCOD_TAMANHO_LOTE,
            COD_FORNECEDOR          = @pCOD_FORNECEDOR,
            COD_RESULTADO_INSPECAO_LOTE = @pCOD_RESULTADO_INSPECAO_LOTE,
            COD_USU_ALT             = SUSER_NAME()+'/'+HOST_NAME(),
            DAT_ALT             = getdate()--,
--	PROJETO REDESENHO MES - ATIVIDADE: 1.4
--             COD_USU_EXC             = Null,
--             DAT_EXC             = Null

    WHERE   
        COD_LOTE            = @pCOD_LOTE

    -- Se a data de fim de lote foi alterada, alterar os campos dat_historico de todas as coletas
    -- para que todos os registros sejam processados pelo extrator

    -- Se o lote foi fechado com data anterior a atual tambÚm muda para garantir que o lote seja
    -- selecionado pelo extrator

    -- Obtem dat_historico

    select @dat_historico = convert(datetime,convert(varchar,getdate(),106))

    if ((@dat_fim_lote is not null) and (@dat_fim_lote != @pdat_fim_lote)) or
       ((@dat_fim_lote is null    ) and (@pdat_fim_lote < @dat_historico)) or
       ((@dat_fim_lote is not null) and (@cod_resultante_processo != @pcod_resultante_processo)) or
       ((@dat_fim_lote is not null) and (@cod_fornecedor != @pcod_fornecedor)) or
       ((@dat_fim_lote is not null) and (@cod_resultado_inspecao_lote != @pcod_resultado_inspecao_lote)) or
       ((@dat_fim_lote is not null) and (@cod_tamanho_lote != @pcod_tamanho_lote))
       begin

        -- Atualiza o campo dat_historico da tabela coleta_normal
        
           UPDATE COLETA_NORMAL
                SET     DAT_HISTORICO = @DAT_HISTORICO				
		        ,     cod_usu_alt = SUSER_NAME()+'/'+HOST_NAME()
                WHERE COD_LOTE = @pCOD_LOTE
--	PROJETO REDESENHO MES - ATIVIDADE: 1.4
--              AND DAT_EXC IS NULL

        -- Atualiza o campo dat_historico da tabela coleta_periodico
        
           UPDATE COLETA_PERIODICO
                SET     DAT_HISTORICO = @DAT_HISTORICO				
				  ,     cod_usu_alt = SUSER_NAME()+'/'+HOST_NAME()
                WHERE COD_LOTE = @pCOD_LOTE
--	PROJETO REDESENHO MES - ATIVIDADE: 1.4
--              AND DAT_EXC IS NULL

        -- Atualiza o campo dat_historico da tabela coleta_lote_origem
        
           UPDATE COLETA_LOTE_ORIGEM
                SET     DAT_HISTORICO = @DAT_HISTORICO				
				,     cod_usu_alt = SUSER_NAME()+'/'+HOST_NAME()
                WHERE COD_LOTE = @pCOD_LOTE
--	PROJETO REDESENHO MES - ATIVIDADE: 1.4
--              AND DAT_EXC IS NULL

       end

-- PROJETO REDESENHO
-- EXCLUSAO DOS REGISTROS DO LOTE NAO ASSOCIADOS A RESULTANTE DE PROCESSO
-- SOMENTE NO CASO DE MUDANCA DA RESULTANTE

--  		  --Verificar ser foi alterado resultante processo  
--		  if exists(select 1
--		              FROM lote
--				      WHERE cod_lote                = @pcod_lote
--				      AND   cod_resultante_processo != @pcod_resultante_processo )

  		  --Verificar ser foi alterado resultante processo   
		  if @cod_resultante_processo != @pcod_resultante_processo

		  BEGIN
                
                -- Se foi alterado resultante de processo excluir logicamente os
                -- registros da coleta normal, coleta periodico e  coleta lote origem
                -- que esta associado com resultante item planilha

		-- Preenche a tabela com itens da coleta normal
		   insert into #item_resultante
			select cod_coleta_normal,
			       'N'
  		    	From   coleta_normal
				Where  coleta_normal.cod_lote = @pcod_lote			
				And    coleta_normal.cod_item_planilha not in ( Select resultante_item_planilha.cod_item_planilha 
				                                                from   resultante_item_planilha
				                                                Where  resultante_item_planilha.cod_item_planilha       = coleta_normal.cod_item_planilha 
				                                                And    resultante_item_planilha.cod_resultante_processo = @pcod_resultante_processo
										And    resultante_item_planilha.dat_exc is null)

		-- Preenche a tabela com itens da coleta de lote origem
		   insert into #item_resultante
			select cod_coleta_lote_origem,
			       'L'
  		    	From   coleta_lote_origem
				Where  coleta_lote_origem.cod_lote = @pcod_lote
				And    coleta_lote_origem.cod_item_planilha not in ( Select resultante_item_planilha.cod_item_planilha 
				                                                     from   resultante_item_planilha
				                                                     Where  resultante_item_planilha.cod_item_planilha       = coleta_lote_origem.cod_item_planilha 
				                                                     And    resultante_item_planilha.cod_resultante_processo = @pcod_resultante_processo
										     And    resultante_item_planilha.dat_exc is null)
	
		-- Preenche a tabela com itens da coleta periodica
		   insert into #item_resultante
			select cod_coleta_periodico,
			       'P'
  		    	From   coleta_periodico
				Where  coleta_periodico.cod_lote = @pcod_lote
				And    coleta_periodico.cod_item_planilha not in ( Select resultante_item_planilha.cod_item_planilha 
				                                                     from   resultante_item_planilha
				                                                     Where  resultante_item_planilha.cod_item_planilha       = coleta_periodico.cod_item_planilha 
				                                                     And    resultante_item_planilha.cod_resultante_processo = @pcod_resultante_processo
										     And    resultante_item_planilha.dat_exc is null)
		-- Declara cursor para ler a tabela temporaria

		   declare @cod_coleta_cr 	integer
		   declare @dsc_coleta_cr	char(1)
--		   declare @dat_historico	datetime
		   declare @nom_coleta		varchar(20)
		   declare @count			integer
--		   declare @error			integer

		   select @dat_historico = convert(datetime,convert(varchar,getdate(),106))

  		   declare cursor_crs01 cursor for 
				select cod_coleta,dsc_coleta from #item_resultante
    		   for read only 

		-- Abre cursor

  		   open cursor_crs01
  		   fetch cursor_crs01 into @cod_coleta_cr, @dsc_coleta_cr

		-- Faz o loop chamando a rotina de delecao para deletar os registros respectivos

 		   while @@sqlstatus=0
    		   begin

			-- Deleta o registro de acordo com o tipo de coleta

			if @dsc_coleta_cr = 'N'
				begin
					execute sp_de_coleta_normal @cod_coleta_cr,null,null,null,@dat_historico,null,@count output, @error output
					select @nom_coleta = 'COLETA_NORMAL'
				end

			if @dsc_coleta_cr = 'L'
				begin
					execute sp_de_coleta_lote_origem @cod_coleta_cr,null,null,null,null,@dat_historico,null,@count output, @error output
					select @nom_coleta = 'COLETA_LOTE_ORIGEM'
				end

			if @dsc_coleta_cr = 'P'
				begin
					execute sp_de_coleta_periodico @cod_coleta_cr,null,null,null,@dat_historico,null,null,@count output, @error output
					select @nom_coleta = 'COLETA_LOTE_PERIODICO'
				end

			IF @COUNT = 0
			BEGIN
				EXEC SP_SE_ERRO '3M', '50004', @nom_coleta
				RETURN
			END
			IF @COUNT = -1
			BEGIN
				EXEC SP_SE_ERRO '3M', '50050', @nom_coleta
				RETURN
			END
			IF @COUNT = -2
			BEGIN
				EXEC SP_SE_ERRO '3M', '50020', @nom_coleta
				RETURN
			END

			-- Le pr¾xima linha
  		   	fetch cursor_crs01 into @cod_coleta_cr, @dsc_coleta_cr

		   end

		   close cursor_crs01 
		   deallocate cursor cursor_crs01 

		  END

IF @@ERROR <> 0 GOTO ERRO 
COMMIT TRANSACTION TRANS_LOTE
Return 
 
ERRO: 
   ROLLBACK TRANSACTION TRANS_LOTE
   EXEC SP_SE_ERRO 'AD','30000','ERRO NA ATUALIZAÃ┬O DOS DADOS - LOTE' 
   return;
